#pragma once

#include "circular_list.h"

template <typename T>
class Queue
{
  private:
    CircularList<T> _list;

  public:
    bool isEmpty() const {
        return this->_list.isEmpty();
    }

    void enqueue(T value)
    {
        this->_list.push(value);
    }

    T dequeue() {
        return this->_list.pop();
    }
};