#pragma once

#include <math.h>

#include "queue.h"

template <typename T>
T max(T a, T b)
{
    return a > b ? a : b;
}

template <typename T>
class BinTree
{
  private:
    class BinTreeNode;
    BinTree<T>::BinTreeNode *_root;

  public:
    BinTree() : _root(NULL) {}
    ~BinTree() { delete _root; }

    void insert(T value)
    {
        this->_root = BinTree<T>::BinTreeNode::insertSubtree(this->_root, value);
    }

    void bfs(void (*print)(const T &))
    {
        if (this->_root == NULL)
            return;

        Queue<BinTree<T>::BinTreeNode *> nodes;
        nodes.enqueue(this->_root);

        while (!nodes.isEmpty())
        {
            BinTree<T>::BinTreeNode *node = nodes.dequeue();

            if (node == NULL)
                continue;

            nodes.enqueue(node->left());
            nodes.enqueue(node->right());
            print(node->value());
        }
    }

    void preorder(void (*print)(const T &))
    {
        BinTree<T>::BinTreeNode::preorder(this->_root, print);
    }

  private:
    class BinTreeNode
    {
      private:
        BinTree<T>::BinTreeNode *_left;
        BinTree<T>::BinTreeNode *_right;
        const T _value;

      public:
        BinTreeNode(T value) : _left(NULL), _right(NULL), _value(value) {}

        ~BinTreeNode()
        {
            delete _left;
            delete _right;
        }

        void insert(T value)
        {
            if (value <= this->_value)
                _left = BinTree<T>::BinTreeNode::insertSubtree(_left, value);
            else
                _right = BinTree<T>::BinTreeNode::insertSubtree(_right, value);
        }

        int height() const
        {
            return BinTree<T>::BinTreeNode::safe_height(this);
        }

        BinTree<T>::BinTreeNode *left() const { return this->_left; }
        BinTree<T>::BinTreeNode *right() const { return this->_right; }
        T value() const { return this->_value; }

      public:
        static BinTree<T>::BinTreeNode *insertSubtree(BinTree<T>::BinTreeNode *node, T value)
        {
            if (node == NULL)
                return new BinTreeNode(value);

            node->insert(value);
            return node;
        }

        static void preorder(BinTree<T>::BinTreeNode *node, void (*print)(const T &))
        {
            if (node == NULL)
                return;

            BinTree<T>::BinTreeNode::preorder(node->_left, print);

            print(node->_value);

            BinTree<T>::BinTreeNode::preorder(node->_right, print);
        }

      private:
        static int safe_height(const BinTree<T>::BinTreeNode *node)
        {
            if (node == NULL)
                return 0;

            return max(BinTree<T>::BinTreeNode::safe_height(node->_left), BinTree<T>::BinTreeNode::safe_height(node->_right)) + 1;
        }
    };
};