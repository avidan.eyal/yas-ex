#pragma once

template <typename T>
class CircularList
{
  private:
    class ListNode;
    CircularList<T>::ListNode *_head;

  public:
    CircularList() : _head(NULL) {}
    ~CircularList() { delete _head; }

    bool isEmpty() const
    {
        return this->_head == NULL;
    }

    void push(T value)
    {
        if (this->_head == NULL)
            this->_head = new CircularList<T>::ListNode(value);
        else
            this->_head->insertBefore(value);
    }

    T pop()
    {
        T value = this->_head == NULL ? NULL : this->_head->value();
        this->_head = this->_head->pop();

        return value;
    }

  private:
    class ListNode
    {
      private:
        CircularList<T>::ListNode *_prev;
        CircularList<T>::ListNode *_next;
        const T _value;

      private:
        ListNode(T value, CircularList<T>::ListNode *prev, CircularList<T>::ListNode *next) : _prev(prev), _next(next), _value(value)
        {
            prev->_next = this;
            next->_prev = this;
        }

      public:
        ListNode(T value) : ListNode(value, this, this) {}

        ~ListNode()
        {
            delete _prev;
            delete _next;
        }

        CircularList<T>::ListNode* pop()
        {
            if (this->_next == this)
                return NULL;

            this->_next->_prev = this->_prev;
            this->_prev->_next = this->_next;

            return this->_next;
        }

        void insertBefore(T value)
        {
            new CircularList<T>::ListNode(value, this->_prev, this);
        }

        T value() const {
            return this->_value;
        }
    };
};