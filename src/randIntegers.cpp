#include <stdio.h> 
#include <stdlib.h>
#include <time.h>   

#include "randIntegers.h"

int _randInt(int min, int max) {
    return rand() % (max - min) + min;
}

std::vector<int> randIntegers(int count, int min, int max) {
    srand (time(NULL));
    
    std::vector<int> values(count);

    for (int i = 0; i < count; ++i)
        values[i] = _randInt(min, max);

    return values;
}