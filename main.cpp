#include <algorithm>
#include <iostream>

#include "src/randIntegers.h"
#include "src/bintree.h"

#define MIN_NUM 1
#define MAX_NUM 1000
#define NUM_COUNT 50

int main() {
    auto print = [](const int& n) { std::cout << n << " "; };
    
    auto values = randIntegers(NUM_COUNT, MIN_NUM, MAX_NUM);

    std::cout << "numbers:  ";
    std::for_each(values.begin(), values.end(), print);
    std::cout << std::endl << std::endl;

    BinTree<int> tree;
    std::for_each(values.begin(), values.end(), [&tree](const int& n) { tree.insert(n); });    
    
    std::cout << "preorder: ";    
    tree.preorder(print);
    std::cout << std::endl << std::endl;
    
    std::cout << "bfs:      ";    
    tree.bfs(print);
    std::cout << std::endl << std::endl;
    
    return 0;
}